#!/usr/bin/env python

"""
    carpino-web.py part of the carpino project.
    Copyright (C) 2017 Georgi Kolev <georgi.kolev gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import json
import config
from tornado import websocket, web, ioloop

__version__ = '0.999a1'
__license__ = 'GPLv3'
__author__ = 'georgi.kolev<at>gmail.com'
__name__ = 'carpino-web'

# Try to include procname module
try: from procname import setprocname
except: print('warn: procname module not found.')
else: setprocname(__name__)


class IndexHandler(web.RequestHandler):
    def get(self): self.render('index.html')


class SocketHandler(websocket.WebSocketHandler):
    def check_origin(self, origin): return True

    def open(self):
        if self not in cl:
            cl.append(self)

    def on_message(self, message):
        for c in cl:
            c.write_message(message)

    def on_close(self):
        if self in cl:
            cl.remove(self)


class ApiHandler(web.RequestHandler):
    @web.asynchronous
    def get(self, *args):
        try:
            id = self.get_argument('id')
            value = self.get_argument('value')
            data = json.dumps({'id': id, 'value': value})
            for c in cl:
                c.write_message(data)
        except:
            print('MSG Error')
        self.redirect('/', permanent=True)

    @web.asynchronous
    def post(self): pass


cl = []
settings = {
    'static_path': os.path.join(config.root_dir, 'static'),
    'compress_response': True,
}
app = web.Application([
    (r'/', IndexHandler),
    (r'/api', ApiHandler),
    (r'/ws', SocketHandler),
    ], **settings)

# Like a main loop
app.listen(config.web_port)
ioloop.IOLoop.instance().start()
# eof
