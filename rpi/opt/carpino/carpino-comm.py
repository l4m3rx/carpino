#!/usr/bin/env python

"""
    carpino-comm.py part of the carpino project.
    Copyright (C) 2017 Georgi Kolev <georgi.kolev gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import json
import config
from os import system
from time import sleep
from mpd import MPDClient
from serial import Serial
from threading import Lock
from thread import start_new_thread
from websocket import create_connection

__version__ = '0.999a1'
__license__ = 'GPLv3'
__author__ = 'georgi.kolev<at>gmail.com'
__name__ = 'carpino-comm'

# Try to include procname module
try: from procname import setprocname
except: print('warn: procname module not found.')
else: setprocname(__name__)

commands = ['keyfob', 'bt_voice', 'bt_phone']


class LockableMPDClient(MPDClient):
    def __init__(self, use_unicode=False):
        super(LockableMPDClient, self).__init__()
        self.use_unicode = use_unicode
        self._lock = Lock()

    def __exit__(self, type, value, traceback): self.release()

    def acquire(self): self._lock.acquire()

    def release(self): self._lock.release()

    def __enter__(self): self.acquire()


def help_thread(ws, mc, wlock):
    while True:
        with wlock:
            ws.send(pack_data(get_temp()))
        try:
            with wlock:
                ws.send(pack_data(get_track(mc)))
        except UnicodeDecodeError as e:
            pass
        mc.ping()
        sleep(3)


def get_dname(csong):
    if ('artist' in csong) and ('title' in csong):
        csong = '%s - %s' % (csong['artist'], csong['title'])
    else:
        csong = csong['file']
    return csong[:64].replace("'", "")


def get_track(mpdconn, csong='none'):
    with mpdconn:
        csong = mpdconn.currentsong()
        csong = 'track_name=' + get_dname(csong)
    return csong

def pack_data(sdata):  # Make data into dict
    try: sdata = sdata.split('=')
    except: return False
    else: return {u'id': sdata[0], u'value': sdata[1]}


def get_temp():  # Get CPU temp
    with open(config.cputemp_file) as f:
        return 'cpu_temp=' + str(int(f.read())/100)


def conn2serial(): return Serial(config.serial_port, config.serial_speed)
def conn2websock(): return create_connection(config.websock_addr)
def shutdown(): return system('sudo /sbin/halt')

def mpd_prev(mc):
    with mc: mc.previous()

def mpd_next(mc):
    with mc: mc.next()


error_count = 0
wlock = Lock()              # WebSocket Lock
sport = conn2serial()       # Serial Connection
websock = conn2websock()    # WebSocket Connection
# Lockable MPD Client
mpclient = LockableMPDClient()
mpclient.connect('localhost', 6600)
# Helper thread
start_new_thread(helper_thread, (websock, mpclient, wlock))


# Main loop
while True:
    try:
        buffer = sport.readline().strip()   # Read line from serial
        buffer = pack_data(buffer)	        # Make into dict
        if buffer:				            # Any usable data?
            if (buffer['id'] == 'keyfob') and (buffer['value'] == 'lock'):
                shutdown()                  # Start shutting down
            elif (buffer['id'] == 'bt_voice') and (buffer['value'] == '1'):
                mpd_next()                  # MPD Next Track
            elif (buffer['id'] == 'bt_phone') and (buffer['value'] == '1'):
                mpd_prev()                  # MPD Prev Track
        websock.send(json.dumps(buffer))    # Send as json to websocket
        error_count = 0
    except:
        error_count += 1
    if error_count > 16:
        print("Error count > 16. Quiting")
        sys.exit(1)

# eof
