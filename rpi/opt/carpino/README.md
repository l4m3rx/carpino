# carpino-comm.py

--------------------------------------
> * The primary function of this deamon is to get the messages from
>   the Arudino (can bus) and make 'em in to json array before
>   sending them to the websocket
> * The secondary function is to watch for specific messages and
>   do something. For now, start shutdown if keyfob lock, and the
>   audio/mpd control from the steering wheel buttons.
> * To keep the mpd socket conneciton alive, we are sending "ping"
>   requests every 3 seconds.
--------------------------------------

# carpino-web.py

----------------------------------------
> * Web server
>   With simple dashboard like UI.
> * Web socket server
> * API to push data
----------------------------------------
![1](https://bytebucket.org/l4m3rx/carpino/raw/d4961dea7aa000d99a1999fde460409966d73d9b/resources/1.gif)
![2](https://bytebucket.org/l4m3rx/carpino/raw/d4961dea7aa000d99a1999fde460409966d73d9b/resources/2.gif)
![3](https://bytebucket.org/l4m3rx/carpino/raw/d4961dea7aa000d99a1999fde460409966d73d9b/resources/3.gif)
![4](https://bytebucket.org/l4m3rx/carpino/raw/ef00ada9c08012dbbe64cb95a638e719b3332353/resources/4.gif)
----------------------------------------
