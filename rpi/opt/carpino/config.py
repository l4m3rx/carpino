web_port = 7350
serial_speed = 100000
root_dir = '/opt/carpino'
serial_port = '/dev/ttyUSB0'
websock_addr = 'ws://127.0.0.1:%s/ws' % web_port
cputemp_file = '/sys/class/thermal/thermal_zone0/temp'
