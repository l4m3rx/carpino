#define SERIAL_SPEED    115200

#define B_THRESHOLD     500
#define PASS_LEN        6

#define CLEAR_CHAR      114
#define MEMORY_CHAR     109

#define EEPROM_MIN      16
#define EEPROM_OFFSET   2

#define BUZZER_PIN  21
#define FPR_PIN     12
#define CS_PIN      17

#define B_VOICE     0
#define B_PHONE     1
#define B_VOLUP     2
#define B_TUNEUP    3
#define B_VOLDOWN   4
#define B_CHANGER   5
#define B_AIRFLOW   6
#define B_TUNEDOWN  7

#define MPG2KM1KL   235.215

#define TRUE    1
#define FALSE   0

typedef struct CanMessage {
    INT32U id;
    INT8U len;
    INT8U data[8];
};

typedef struct CarValues {
    char datetime[20];
    float in_temp, ext_temp;
    float left_temp, right_temp;
    float speed, avr_lkm, avr_kmh;
    short rpm, range, coolant;
    long swheel, freemem, odometer;
    byte fuel, fan_speed, throttle;
    byte fl_window, fr_window, rl_window, rr_window;
};

typedef struct CarStatus {
  union {
    unsigned char value[5];
    struct {
      unsigned key:1;
      unsigned locked:1;
      unsigned engine:1;
      unsigned reverse:1;
      unsigned lockdown:1;
      unsigned handbrake:1;
      unsigned fuelrelay:1;
      // Door status open/lock
      unsigned fl_door:1; // 8
      unsigned fr_door:1;
      unsigned rl_door:1;
      unsigned rr_door:1;
      unsigned fl_lock:1;
      unsigned fr_lock:1;
      unsigned rl_lock:1;
      unsigned rr_lock:1;
      // Hood status open
      unsigned hood:1; // 16
      // Boot status open/lock
      unsigned boot:1;
      unsigned boot_lock:1; // TBD
      // Low/Main/High Beams
      unsigned low_beam:1;
      unsigned main_beam:1;
      unsigned high_beam:1; // TBD
      // Fog lights
      unsigned rear_fog:1;
      unsigned front_fog:1;
      // Others
      unsigned term15:1; // 24
      unsigned term30:1;
      unsigned clutch:1;
      unsigned brakes:1;
      unsigned demister:1;
      unsigned door_alarm:1;
      unsigned aircondition:1;
      unsigned airconditionmax:1;
      unsigned recirculate:1; // 32
      unsigned recirculateauto:1;
      unsigned hazard_lights;
      unsigned left_turnsignal;
      unsigned right_turnsignal;
    };
  };
} __attribute__((packed));

