// Those are too short for functions so ...
#define LOW_BITS(b)  (b & 0xF)
#define HIGH_BITS(b) ((b>>4) & 0xF)
#define GET_STATE_ADDR eeprom_read_int(0)


// Read int from EEPROM
unsigned int eeprom_read_int(int p_address) {
   byte lowByte  = EEPROM.read(p_address);
   byte highByte = EEPROM.read(p_address + 1);

   return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}

// Write int to EEPROM
void eeprom_write_int(int p_address, int p_value) {
   byte lowByte  = ((p_value >> 0) & 0xFF);
   byte highByte = ((p_value >> 8) & 0xFF);

   EEPROM.write(p_address, lowByte);
   EEPROM.write(p_address + 1, highByte);
}

bool get_eeprom_state() {
  return EEPROM.read(GET_STATE_ADDR);
}

void write_eeprom_state(bool state) {
  unsigned char w_count;

  int max_addr = EEPROM.length() - 1;
  int state_addr = GET_STATE_ADDR;
  int count_addr = state_addr + 1;

  // Write stuff only when there is change
  if (state == get_eeprom_state())
    return;

  // Where should we write this?
  w_count = EEPROM.read(count_addr);
  if (w_count == 0xFF) {
    w_count = 0;
    state_addr = state_addr + EEPROM_OFFSET;
    if (state_addr >= max_addr)
      state_addr = EEPROM_MIN;
    // Write new state address
    eeprom_write_int(0, state_addr);
  }
  w_count++;
  // Write state & write count to EEPROM
  EEPROM.write(state_addr , state);
  EEPROM.write((state_addr + 1), w_count);
}

void print_can() {
  Serial.print(message.id, HEX);
  Serial.print(" - ");
  for(int i = 0; i < message.len; i++) {
      Serial.print(message.data[i], HEX);
      Serial.print(" ");
  }
  Serial.println();
}

void check_serial() {
  if (Serial.available()) {
    int sch = Serial.read();
    serial_msg(sch);
  }
}

void lockdown_check() {
  if ((!car.key) && car.fuelrelay && (!car.engine))
    fpr_control(FALSE);
  if (car.key && car.lockdown && (!car.fuelrelay))
    fpr_control(TRUE);
  if ((!car.key) && car.engine && (!car.fuelrelay))
    fpr_control(TRUE);
}

void engine_status() {
  if ((carv.rpm > 0) && (!car.engine))
    car.engine = scheck("engine", TRUE, car.engine);
  else if ((!carv.rpm) && car.engine)
    car.engine = scheck("engine", FALSE, car.engine);
}

void fpr_control(boolean s) {
  if (car.fuelrelay != s) {
    car.fuelrelay = scheck("fuel_relay", s, car.fuelrelay);
    memset(&stack_array, 0, (sizeof(int) * PASS_LEN));    
  }
  digitalWrite(FPR_PIN, car.fuelrelay);
}

// Add to button stack
void button_stack(int p) {
  if (car.lockdown) {
    stack_push(p);
    if (arrcmp(unlock_code, stack_array, PASS_LEN)) {
      Serial.println("auth=true");
      car.lockdown = FALSE;
      fpr_control(FALSE);
      // Preserve state in EEPROM
      write_eeprom_state(FALSE);
      tone(BUZZER_PIN, 6000, 100);
#ifdef DEBUG
    } else {
      Serial.print("arrcmp(");
      for (int n=0; n<PASS_LEN; n++)
        Serial.print(stack_array[n]);
      Serial.print(", ");
      for (int n=0; n<PASS_LEN; n++)
        Serial.print(unlock_code[n]);
      Serial.println(")");
#endif
    }
  }
}

// LIFO Stack Push
void stack_push(int p) {
  int temp_array[PASS_LEN-1];
  // We trust gcc, right?
  memcpy(&temp_array,    &stack_array, (sizeof(int) * (PASS_LEN-1)));
  memcpy(&stack_array[1], &temp_array, (sizeof(int) * (PASS_LEN-1)));
  // Add the new element as the first element in the array
  stack_array[0] = p;
}

boolean arrcmp(int a[], int b[], int n) {
  int i = 0;
  boolean c = true;

  while(i < n && c) {
      c = a[i] == b[i];
      i++;
  }
  return c;
}

// Short/Long Button Press
int long_press(unsigned long ptime) {
  return ((millis() - ptime) > B_THRESHOLD)? 2 : 1;
}

long scheck(char pstr[], long nval, long cval) {
  if (cval != nval)
    pprint(pstr, nval, FALSE);
  return nval;
}

float vcheck(char pstr[], float nval, float cval) {
  if (cval != nval)
    pprint(pstr, FALSE, nval);
  return nval;
}

// Custom print function
void pprint(char pstr[], long pint, float pdig) {
  if (pdig == FALSE)
    serial.printf("%s=%d\n", pstr, pint);
  else
    serial.printf("%s=%4.2f\n", pstr, pdig);
  Serial.flush();
}


// Read data from serial and act on it
void serial_msg(int ch) {
  if (CLEAR_CHAR == ch)
    clear_struct();
  else if (MEMORY_CHAR == ch)
    carv.freemem = scheck("free_memory", free_mem(), carv.freemem);
}

// Check free memory
int free_mem() {
    int v;
    extern int __heap_start, *__brkval;
    return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

// Clear the structures but dont null the init & lockdown values.
void clear_struct() {
  car.key = 0;
  car.locked = 0;
  car.engine = 0;
  car.reverse = 0;
  car.handbrake = 0;
  car.fuelrelay = 0;
  car.fl_door = 0;
  car.fr_door = 0;
  car.rl_door = 0;
  car.rr_door = 0;
  car.fl_lock = 0;
  car.fl_lock = 0;
  car.rl_lock = 0;
  car.rr_lock = 0;
  car.hood = 0;
  car.boot = 0;
  car.boot_lock = 0;
  car.low_beam = 0;
  car.main_beam = 0;
  car.high_beam = 0;
  car.rear_fog = 0;
  car.front_fog = 0;
  car.term15 = 0;
  car.term30 = 0;
  car.clutch = 0;
  car.brakes = 0;
  car.demister = 0;
  car.door_alarm = 0;
  car.aircondition = 0;
  car.airconditionmax = 0;
  car.recirculate = 0;
  car.recirculateauto = 0;
  car.hazard_lights = 0;
  car.left_turnsignal = 0;
  car.right_turnsignal = 0;
  carv.rpm = 0;
  carv.fuel = 0;
  carv.speed = 0;
  carv.range = 0;
  carv.in_temp = 0;
  carv.coolant = 0;
  carv.freemem = 0;
  carv.ext_temp = 0;
  carv.odometer = 0;
  carv.throttle = 0;
  carv.left_temp = 0;
  carv.fan_speed = 0;
  carv.fl_window = 0;
  carv.fr_window = 0;
  carv.rl_window = 0;
  carv.rr_window = 0;
  carv.right_temp = 0;
  memset(&carv.datetime, 0, sizeof carv.datetime);
  Serial.println("structs=cleared");
}
