/*
    carpino.ino - BMW e87 K-CAN Traffic Parsing
    Copyright (C) 2017-2018 Georgi Kolev <georgi _at_ slackware.bg>

    Licensed under the EUPL, Version 1.2 or – as soon they will be
    approved by the European Commission - subsequent versions of the
    EUPL (the "Licence");
    You may not use this work except in compliance with the Licence.
    You may obtain a copy of the Licence at:

      https://joinup.ec.europa.eu/software/page/eupl

    Unless required by applicable law or agreed to in writing,
    software distributed under the Licence is distributed on an "AS IS"
    basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
    or implied. See the Licence for the specific language governing
    permissions and limitations under the Licence.

    Credits should mostly go to Trevor Cook <http://loopybunny.co.uk/>
    For decoding the K-CAN traffic. In the end, this is the hard work.
    And to Thaniel from bimmerforums... reading his work helped a lot!

    Really sorry for the code... sadly, I can't do better
*/

#define DEBUG

#include <SPI.h>
#include <EEPROM.h>
#include <PrintEx.h>
#include <mcp_can.h>
#include "carpino.h"


// Set MCP CS Pin
MCP_CAN CAN(CS_PIN);
// Serial wrapper
PrintEx serial = Serial;
// Structs
struct CarStatus car;
struct CarValues carv;
struct CanMessage message;
// Variables for the lock
unsigned int stack_array[PASS_LEN];
unsigned long bt_voice, bt_airflow, bt_tuneup, bt_voldown;
unsigned long bt_phone, bt_changer, bt_volup, bt_tunedown;
const int unlock_code[PASS_LEN] = \
  {B_VOICE, B_CHANGER, B_PHONE, B_VOICE, B_CHANGER, B_PHONE};


// Initial Setup
void setup() {
    pinMode(FPR_PIN, OUTPUT);
    pinMode(BUZZER_PIN, OUTPUT);

    Serial.begin(SERIAL_SPEED);
    Serial.print("Init...");

    while (CAN_OK != CAN.begin(CAN_100KBPS)) {
      Serial.print("FAIL ");
      delay(100);
    }

    Serial.println("OK");
    tone(BUZZER_PIN, 6000, 200);

    car.lockdown = get_eeprom_state();
    carv.freemem = scheck("free_memory", free_mem(), carv.freemem);
}


// Main Loop
void loop(){

  // Fuel relay lock/unlock
  lockdown_check();
  // Read incoming data from serial.
  check_serial();

  // Any new CAN messages to read?
  if (CAN_MSGAVAIL == CAN.checkReceive())
  {
      CAN.readMsgBuf(&message.len, message.data);
      message.id = CAN.getCanId();
//#ifdef DEBUG
//      print_can(&message);
//#endif
      switch (CAN.getCanId())
      {
        case 0x1D6: // Steering wheel buttons
          msg_0x1D6(message.data);
          break;
        case 0x23A: // Keyfob
          msg_0x23A(message.data);
          break;
        case 0x3B0: // Rerverse gear
          msg_0x3B0(message.data);
          break;
        case 0x32E: // Internal Temperature
          msg_0x32E(message.data);
          break;
        case 0x1D0: // Coolant
          msg_0x1D0(message.data);
          break;
        case 0xAA: // RPM, Throttle
          msg_0xAA(message.data);
          engine_status();
          //driving_reboot(&car, &carv);
          break;
        case 0x1B4: // Speed, Handbreak
          msg_0x1B4(message.data);
          break;
        case 0x330: // Odometer, Fuel
          msg_0x330(message.data);
          break;
        case 0x34F: // Handbreak
          msg_0x34F(message.data);
          break;
        case 0x130: // Engine & Key Status
          msg_0x130(message.data);
          //chk_init(&car);
          break;
        case 0x24B: // Door Alarms
          msg_0x24B(message.data);
          break;
        case 0x21A: // Lights
          msg_0x21A(message.data);
          break;
        case 0x246: // Rear demister
          msg_0x246(message.data);
          break;
        case 0x2E6: // Left clima temp & Fan speed
          msg_0x2E6(message.data);
          break;
        case 0x2EA: // Right clima temp
          msg_0x2EA(message.data);
          break;
        case 0x366: // External Temp, Range
          msg_0x366(message.data);
          break;
        case 0x2FC: // Door & Boot Status
          msg_0x2FC(message.data);
          break;
        case 0x3B6: // Front left window
          msg_0x3B6(message.data);
          break;
        case 0x3B7: // Rear left window
          msg_0x3B7(message.data);
          break;
        case 0xC8: // Steering wheel angle (WIP)
          msg_0xC8(message.data);
          break;
        case 0x3B8: // Front reft window
          msg_0x3B8(message.data);
          break;
        case 0x3B9: // Rear right window
          msg_0x3B9(message.data);
          break;
        case 0x1F6: // turn signal/Hazzard Lights (WIP)
          msg_0x1F6(message.data);
          break;
        case 0x1EE: // Indicator Stalk position (WIP)
          msg_0x1EE(message.data);
          break;
        case 0x362: // avrg. speed & fuel consumation
          msg_0x362(message.data);
          break;
        case 0x380: // VIN
          msg_0x380(message.data);
          break;
        case 0x2F8: // Time & Date
          msg_0x2F8(message.data);
          break;
        case 0x242: // AC Status, Air Recirculation
          msg_0x242(message.data);
          break;
        case 0x26E: // Term15 & Term30
          msg_0x26E(message.data);
          break;
        //case 0xF2:  // Boot Lock (Missing in e87?)
        //case 0x252: // Windscreen Wiper Status (TBD)
        //case 0x2A6: // Windscreen Wiper Controls (TBD)
        //case 0x349: // Left/Right Fuel Sensor (TBD)
        //case 0x328: // Battery Reset, Production date (TBD)
        default: // When done with the known traffic we should start printing what is left
          break;
      }
    }
}

