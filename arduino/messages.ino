// Steering wheel buttons
void msg_0x1D6(INT8U msg[8]) {
  if (bitRead(msg[0], 3) && (!bt_volup)) {
    bt_volup = millis();
  } else if ((!bitRead(msg[0], 3)) && bt_volup) {
    pprint("bt_volup", long_press(bt_volup), 0);
    button_stack(B_VOLUP);
    bt_volup = FALSE;
  }
  if (bitRead(msg[0], 2) && (!bt_voldown)) {
    bt_voldown = millis();
  } else if ((!bitRead(msg[0], 2)) && bt_voldown) {
    pprint("bt_voldown", long_press(bt_voldown), 0);
    button_stack(B_VOLDOWN);
    bt_voldown = FALSE;
  }
  if (bitRead(msg[1], 0) && (!bt_voice)) {
    bt_voice = millis();
  } else if ((!bitRead(msg[1], 0)) && bt_voice) {
    pprint("bt_voice", long_press(bt_voice), 0);
    button_stack(B_VOICE);
    bt_voice = FALSE;
  }
  if (bitRead(msg[0], 0) && (!bt_phone)) {
    bt_phone = millis();
  } else if ((!bitRead(msg[0], 0)) && bt_phone) {
    pprint("bt_phone", long_press(bt_phone), 0);
    button_stack(B_PHONE);
    bt_phone = FALSE;
  }
  if (bitRead(msg[1], 6) && (!bt_changer)) {
    bt_changer = millis();
  } else if ((!bitRead(msg[1], 6)) && bt_changer) {
    pprint("bt_changer", long_press(bt_changer), 0);
    button_stack(B_CHANGER);
    bt_changer = FALSE;
  }
  if (bitRead(msg[1], 4) && (!bt_airflow)) {
    bt_airflow = millis();
  } else if ((!bitRead(msg[1], 4)) && bt_airflow) {
    pprint("bt_airflow", long_press(bt_airflow), 0);
    button_stack(B_AIRFLOW);
    bt_airflow = FALSE;
  }
  if (bitRead(msg[0], 5) && (!bt_tuneup)) {
    bt_airflow = millis();
  } else if ((!bitRead(msg[0], 5)) && bt_tuneup) {
    pprint("bt_tuneup", long_press(bt_tuneup), 0);
    button_stack(B_TUNEUP);
    bt_tuneup = FALSE;
  }
  if (bitRead(msg[0], 4) && (!bt_tunedown)) {
    bt_airflow = millis();
  } else if ((!bitRead(msg[0], 4)) && bt_tunedown) {
    pprint("bt_tunedown", long_press(bt_tunedown), 0);
    button_stack(B_TUNEDOWN);
    bt_tunedown = FALSE;
  }
}

// Rear left window
void msg_0x3B7(INT8U msg[8]) {
  carv.rl_window = scheck("rl_window", msg[0], carv.rl_window);
}

// Front left window
void msg_0x3B6(INT8U msg[8]) {
  carv.fl_window = scheck("fl_window", msg[0], carv.fl_window);
}

// Front left window
void msg_0x3B8(INT8U msg[8]) {
  carv.fr_window = scheck("fr_window", msg[0], carv.fr_window);
}

// Rear right window
void msg_0x3B9(INT8U msg[8]) {
  carv.rr_window = scheck("rr_window", msg[0], carv.rr_window);
}

// Right clima temp
void msg_0x2EA(INT8U msg[8]) {
  carv.right_temp = vcheck("right_temp", (msg[7] / 2.0), carv.right_temp);
}

// Rear demister
void msg_0x246(INT8U msg[8]) {
  car.demister = scheck("demister", bitRead(msg[0], 1), car.demister);
}

// Internal temp
void msg_0x32E(INT8U msg[8]) {
  carv.in_temp = vcheck("in_temp", ((msg[3] / 10) + 6), carv.in_temp);
}

// Reverse gear
void msg_0x3B0(INT8U msg[8]) {
  car.reverse = scheck("reverse", bitRead(msg[0], 7), car.reverse);
}

// Coolant
void msg_0x1D0(INT8U msg[8]) {
  carv.coolant = vcheck("coolant", (msg[0] - 48), carv.coolant);
}

// Keyfob
void msg_0x23A(INT8U msg[8]) {
  if (bitRead(msg[2], 2) && (!car.locked)) {
    car.locked = TRUE;
    car.lockdown = TRUE;
    // Write state to EEPROM
    write_eeprom_state(TRUE);
    Serial.println("keyfob=lock");
  }
  if (bitRead(msg[2], 0) && car.locked) {
    car.locked = FALSE;
    Serial.println("keyfob=unlock");
  }
  if (bitRead(msg[3], 4) && car.boot_lock) {
    car.boot_lock = FALSE;
    Serial.println("keyfob=boot");
  }
}

// RPM, Throttle
void msg_0xAA(INT8U msg[8]) {
  carv.rpm = scheck("rpm", (((msg[5] * 0x100) + msg[4]) / 4), carv.rpm);
  carv.throttle = scheck("throttle", (((msg[3] * 0x100) + msg[2]) / 256), carv.throttle);
}

// Speed, Handbrake
void msg_0x1B4(INT8U msg[8]) {
  carv.speed = vcheck("speed", (((((msg[1] - 0xC0) * 256) + msg[0]) / 16) * 1.6), carv.speed);
  //car.handbrake = scheck("handbrake", bitRead(msg[5], 1), bitRead(car.handbrake,0));
}

// Odometer,  Range and and...
void msg_0x330(INT8U msg[8]) {
  print_can(); // DEBUG
  //carv.range = ("range", (((msg[7] * 256) + msg[6]) / 16), carv.range);
  carv.fuel = scheck("fuel", msg[3], carv.fuel);
  // WIP..some issue here :S
  Serial.println( ((msg[2] * 0x10000) + (msg[1] * 0x100) + msg[0]), HEX); //DEBUG
  carv.odometer = scheck("odometer",
    ((msg[2] * 0x10000) + (msg[1] * 0x100) + msg[0]), carv.odometer);
}

// Handbreak
void msg_0x34F(INT8U msg[8]) {
  car.handbrake = scheck("handbrake", bitRead(msg[0], 6), car.handbrake);
}

// Engine & Key Status
void msg_0x130(INT8U msg[8]) {
  car.key = scheck("key", bitRead(msg[1], 6), car.key);
  car.term15 = scheck("term15", bitRead(msg[0], 2), car.term15);
  car.clutch = scheck("clutch", bitRead(msg[2], 6), car.clutch);
  // Clutch issue?
}

// Door alarm
void msg_0x24B(INT8U msg[8]) {
  if (msg[0] != FALSE)
    car.door_alarm = scheck("door_alarms", TRUE, car.door_alarm);
  else
    car.door_alarm = scheck("door_alarms", FALSE, car.door_alarm);
}

// Lights
void msg_0x21A(INT8U msg[8]) {
  car.brakes = scheck("brakes", bitRead(msg[0], 7), car.brakes);
  car.rear_fog = scheck("rear_fog", bitRead(msg[0], 6), car.rear_fog);
  car.low_beam = scheck("low_beam", bitRead(msg[0], 2), car.low_beam);
  car.front_fog = scheck("front_fog", bitRead(msg[0], 5), car.front_fog);
  car.main_beam = scheck("main_beam", bitRead(msg[0], 0), car.main_beam);
}

// Left clima temp & Fan Speed
void msg_0x2E6(INT8U msg[8]) {
  carv.fan_speed = scheck("fan_speed", msg[5], carv.fan_speed);
  carv.left_temp = vcheck("left_temp", (msg[7] / 2), carv.left_temp);
}

// External temp, Temp, Range
void msg_0x366(INT8U msg[8]) {
  carv.freemem = scheck("free_memory", free_mem(), carv.freemem);
  carv.ext_temp = vcheck("ext_temp", ((msg[0] - 80) / 2.0), carv.ext_temp);
  carv.range = scheck("range", ((((msg[2] * 256) + msg[1]) / 16)), carv.range);
}

// Door & Boot Status
void msg_0x2FC(INT8U msg[8]) {
  car.boot = scheck("boot", bitRead(msg[2], 0), car.boot);
  car.fl_door = scheck("fl_door", bitRead(msg[1], 0), car.fl_door);
  car.fr_door = scheck("fr_door", bitRead(msg[1], 3), car.fr_door);
  car.rl_door = scheck("rl_door", bitRead(msg[1], 5), car.rl_door);
  car.rr_door = scheck("rr_door", bitRead(msg[1], 7), car.rr_door);
}

// Steering wheel angle (WIP)
void msg_0xC8(INT8U msg[8]) {
  // is this enough?
  long long1 = ((( msg[1] * 0x100) + msg[0]) / 23);
  carv.swheel = scheck("steering_wheel", long1, carv.swheel);

  //if ((long1 > -360) && (long1 < 360))
  //  carv.swheel = scheck("steering_wheel", long1, carv.swheel);
  //if ((long2 > -360) && (long2 < 360))
  //  carv.swheel = scheck("steering_wheel", long2, carv.swheel);
}

void msg_0x1F6(INT8U msg[8]) {
  switch(msg[0])
  {
    case 0x91:
      car.left_turnsignal = scheck("left_turnsignal", TRUE, car.left_turnsignal);
      break;
    case 0xA1:
      car.right_turnsignal = scheck("right_turnsignal", TRUE, car.right_turnsignal);
      break;
    case 0xB1:
      car.hazard_lights = scheck("hazard_lights", TRUE, car.hazard_lights);
      break;
    default:
      car.left_turnsignal = scheck("left_turnsignal", FALSE, car.left_turnsignal);
      car.right_turnsignal = scheck("right_turnsignal", FALSE, car.right_turnsignal);
      car.hazard_lights = scheck("hazard_lights", FALSE, car.hazard_lights);
      break;
  }
}

// Indicator stalk position (WIP)
void msg_0x1EE(INT8U msg[8]) {
  car.high_beam = scheck("high_beam", bitRead(msg[0], 4), car.high_beam);
  car.left_turnsignal = scheck("left_turnsignal", bitRead(msg[0], 3), car.left_turnsignal);
  car.right_turnsignal = scheck("right_turnsignal", bitRead(msg[0], 1), car.right_turnsignal);

  switch(msg[0]) {
    case 0x01: Serial.println("right_blink=1"); break;
    case 0x04: Serial.println("left_blink=1"); break;
    case 0x20: Serial.println("highbeam_flash=1"); break;
    default: break;
  }
}

// Average MPH/MPG
void msg_0x362(INT8U msg[8]) {
  float kmh = (((msg[0] * 0x10) + LOW_BITS(msg[1])) / 10);
  float lkm = ((msg[2] + (HIGH_BITS(msg[1]) * 0x100)) * MPG2KM1KL);

  carv.avr_lkm = vcheck("avr_lkm", lkm, carv.avr_lkm);
  carv.avr_kmh = vcheck("avr_kmh", kmh, carv.avr_kmh);
}

// VIN
void msg_0x380(INT8U msg[8]) {
  Serial.print("vin=");
  for (int u=0; u < message.len; u++) {
    char c = (char) msg[u];
    Serial.print(c);
  }
  Serial.println("");
}

// Time & Date
void msg_0x2F8(INT8U msg[8]) {
  Serial.print("datetime=");
  sprintf(carv.datetime, "%02d:%02d:%02d %02d/%02d/%d",
    msg[0], msg[1], msg[2], msg[3], HIGH_BITS(msg[4]), ((msg[6] * 0x100) + msg[5]));
  carv.datetime[19] = '\0';
  Serial.println(carv.datetime);
}

// AC Status , Air recirculation
void msg_0x242(INT8U msg[8]) {
  car.recirculate = scheck("recirculate", bitRead(msg[0], 5), car.recirculate);
  car.aircondition = scheck("aircondition", bitRead(msg[0], 0), car.aircondition);
  car.airconditionmax = scheck("ac_max", bitRead(msg[0], 2), car.airconditionmax);
  car.recirculateauto = scheck("recirculate_auto", bitRead(msg[0], 4), car.recirculateauto);
}

// Term 15 & Term 30
void msg_0x26E(INT8U msg[8]) {
  if (msg[1] != FALSE)
    car.term30 = scheck("term30", TRUE,  car.term30);
  else
    car.term30 = scheck("term30", FALSE, car.term30);
}

// Outside temp (Missing in e87, check for ext_temp)
//void msg_0x2CA(INT8U msg[8], CarValues *cv) {
//  carv.out_temp = vcheck("out_temp", ((msg[0] - 80) / 2.0), carv.out_temp);
//}
