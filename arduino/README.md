# Arduino Code

---------------------------------------
> ## This code is mainly doing 2 things
>  * Parse all known can traffic and send it to the serial port
>  * Control fuel pump relay. Not allowing engine start before code is entered


----------------------------------------
> ### Parsing the CAN traffic
> * About the K-CAN traffic parsing, credits should mostly go to Trevor Cook [loopybunny.co.uk](http://loopybunny.co.uk/)
> * Credits should also go to Thaniel from bimmerforums.com... reading his work helped alot!
-----------------------------------------
> ### Fuel Pump Relay Logic
> * Keyfob Lock Press -> go into lockdown
> * If lockdown and key is detected -> deactivate fuel pump
> * If proper code/pass is entered -> activate fuel pump
> * If arudino reboots:
>  * If no key -> set lockdown to true
>  * If key -> set lockdown && deactivate fuel pump
>  * If engine running -> set lockdown to false
>  * If no key & engine running -> set lockdown to true && deactive fuel pump
-----------------------------------------

> ## Known issues
> * odometer has issue
> * Avrg. fuel consumation is not calculated properly
> * Clutch status error

-----------------------------------------

> ## Dependencies
> Check requirements.txt
