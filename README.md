## carpino ##

*This is not even alfa yet*
----------------------------

This is kind of a CarPC ... well..its a bit more.
Probably the best simple way to explain the project is:
-------------------------------------------------------
> *The project is split in to two main components*
> ## **Leonardo Can Bus** /+1 Relay/
> * The CAN Bus is connected to the K-CAN Bus, parses the data and sends it to the Serial/USB port.
> * The relay controls the fuel pump and requires "code" to start the car.
> ## **RPi 3** /+GPS Module and 4G USB Modem/
> * Wireless AP - Sharing the 4G
> * mpd (+ympd for web interface) - controlled from steering wheel
> * tornado/websocket web interface - dashboard like thinggy
> * UPnP/DLNA Server/Renderrer
--------------------------------------------------------

__I just started moving the CAN message parsing to the Arduino insted of the RPi..__
__but this is how it was looking the last it worked.__

![1](https://bytebucket.org/l4m3rx/carpino/raw/d4961dea7aa000d99a1999fde460409966d73d9b/resources/1.gif)
![2](https://bytebucket.org/l4m3rx/carpino/raw/d4961dea7aa000d99a1999fde460409966d73d9b/resources/2.gif)
![3](https://bytebucket.org/l4m3rx/carpino/raw/d4961dea7aa000d99a1999fde460409966d73d9b/resources/3.gif)
![4](https://bytebucket.org/l4m3rx/carpino/raw/ef00ada9c08012dbbe64cb95a638e719b3332353/resources/4.gif)

--------------------------------------------------------
## License
> Licensed under the EUPL, Version 1.2 or – as soon they will be
>    approved by the European Commission - subsequent versions of the
>    EUPL (the "Licence");
>    You may not use this work except in compliance with the Licence.
>
>    You may obtain a copy of the Licence at:
>        https://joinup.ec.europa.eu/software/page/eupl

